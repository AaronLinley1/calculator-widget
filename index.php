<?php

$userInput = '<input type="text" name="input-value" id="input-value" placeholder="Click here and type your value" />';
$buttonChoice = array('Include VAT', 'Remove VAT');
$inputValue = 0;
$vat = 1.2;
$finalValue = 0;
$ouputValue = 0;
$euro = 0;
$dollar = 0;
$yen = 0;

$resetButton = "Clear Form Fields And Go Again";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
	
	if (isset($_POST["input-value"]) AND isset($_POST["include-vat"])) {

		if (is_numeric($_POST["input-value"])){

			$inputValue = $inputValue + $_POST["input-value"];
			$finalValue = $inputValue * $vat;
			$finalValue = round($finalValue, 2);
			$outputValue = "&pound;" . $finalValue;

			$euro = $finalValue * 1.197;
			$euro = round($euro, 2);

			$dollar = $finalValue * 1.622;
			$dollar = round($dollar, 2);

			$yen = $finalValue * 164.328;
			$yen = round($yen, 2);

		} else { 

			$outputValue = "No valid input.";
		}
	
	} elseif (isset($_POST["input-value"]) AND isset($_POST["remove-vat"])) {
		
		if (is_numeric($_POST["input-value"])){

			$inputValue = $inputValue + $_POST["input-value"];
			$finalValue = $inputValue / $vat;
			$finalValue = round($finalValue, 2);
			$outputValue = "&pound;" . $finalValue;

			$euro = $finalValue * 1.197;
			$euro = round($euro, 2);

			$dollar = $finalValue * 1.622;
			$dollar = round($dollar, 2);

			$yen = $finalValue * 164.328;
			$yen = round($yen, 2);

		} else { 

			$outputValue = "No valid input.";
		}

	}

	if ($outputValue != 0 AND isset($_POST["reset-button"])) {

		$outputValue = 0;
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Calculator Widget</title>

	<link rel="stylesheet" href="assets/css/styles.css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:700|Asap' rel='stylesheet' type='text/css'>
</head>
<body>
	
	<header>
		
		<div class="container">

			<h1>Calculator Widget</h1>

			<p>Use the form below to submit your price (number only) and then choose whether you want to add or subtract VAT from it. It really is that simple!</p>

		</div>

	</header>

	<div class="container">
		
		<form method="post" action"index.php" class="calculator">
			
			<div class="content">

				<label for="input-value">Input Your Value:</label>
				<label for="input-value" class="pound">&pound;</label>
				<?php echo $userInput; ?>

				<input type="submit" id="include-vat" name="include-vat" value="<?php echo $buttonChoice[0]; ?>" />
				<input type="submit" id="exclude-vat" name="remove-vat" value="<?php echo $buttonChoice[1]; ?>" />


				<label>Your Final Price Is:</label>
				<p id="final-price"><?php echo $outputValue; ?></p>

				<label>This converts to (approx):</label>

				<div class="conversion">
					
						<label class="euro">&euro;</label>
						<p class="currency euro"><?php echo $euro; ?></p>
				</div>

				<div class="conversion">

						<label class="dollar">&#36;</label>
						<p class="currency dollar"><?php echo $dollar; ?></p>

				</div>

				<div class="conversion">

						<label class="yen">&yen;</label>
						<p class="currency yen"><?php echo $yen; ?></p>

				</div>
					
				<input type="submit" id="reset-button" name="reset-button" value="<?php echo $resetButton; ?>">

			</div>

		</form>

	</div>

	<footer>
			
		<p>&middot; Designed and developed by <a href="http://www.aaronlinley.co.uk/home">Aaron Linley</a> for <a href="http://www.geniusdivision.com">Genius Division</a> &middot;</p>

		<p>&middot; Built with HTML5, CSS3 and PHP &middot;</p>

	</footer>

</body>
</html>
